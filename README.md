# Tweet Feeling

Aplicativo desenvolvido em teste para vaga de desenvolvedor iOS entre os dias 17/03/2021 e 20/03/2021.

> **OBJETIVO**
>
> Criar um aplicativo que dado um nome de usuário do Twitter deva listar os tweets deste. 
> Quando tocar nos tweets o aplicativo deve indicar se é um tweet sobre felicidade, neutro ou triste.
>

## Requisitos

* iOS 11.0+
* Xcode 12.4+
* Swift 5.3.2+

## Considerações

* Construção de layouts com ViewCoding
* Modelo arquitetural com o padrão MVVM
* Versão mínima do iOS 11 para que possa utilizar assets de cores e centralizar a parte de tema do aplicativo
* Navegação entre telas com o padrão Coordinator para retirar esta responsabilidade dos ViewControllers
* Testes unitários realizados o framework XCTest nativo do XCode
* Separação de funcionalidades por módulos, apesar de ser uma aplicação pequena
* Comunicação entre objetos a partir de delegates
* Aplicação de conceitos do SOLID e Programação orientada a protocolos

## Dependências

Para gerenciamento de dependências, utilizei o Swift Package Manager com as bibliotecas abaixo:

* [Alamofire] - Biblioteca que realiza chamadas de serviços para APIs Rest.
* [Core] - Biblioteca pessoal criada para facilitar o desenvolvimento de testes pois contém implementações comuns em qualquer tipo de aplicação.

[Alamofire]: <https://github.com/Alamofire/Alamofire>
[Core]: <https://gitlab.com/luiswolf/ios-core.git>

## Apresentação da aplicação

### Tweets

Tela principal da aplicação, onde são listados os tweets de determinado usuário, estes, consultados a partir da API do twitter com paginação realizada através de scroll infinito.
    
<br>  
<img src="screenshots/screenshot-01.png" width="300">

### Sentimento

A tela de sentimento exibe o tweet do usuário e também a avaliação de sentimento sobre seu texto a partir da API do Google Natural Language.
    
<br>  
<img src="screenshots/screenshot-04.png" width="300"><img src="screenshots/screenshot-03.png" width="300"><img src="screenshots/screenshot-02.png" width="300">
   
<br>  
<br>  
A classificação do sentimento é baseada na escala abaixo:
   
<br>  
<br>  

* **Triste:** avaliacão entre  -1.0 e -0.24
* **Neutro:** avaliacão entre -0.25 e  0.25
* **Feliz:**  avaliacão entre  0.26 e   1.0

### Alteração de usuário

O processo de alteração não foi um requisito solicitado, mas adicionei com o objetivo de validação da aplicação e verificar que os tweets exibidos nao de fato os do usuário escolhido.
Para este processo, defini uma lista de usuários predefinidos, parametrizados no arquivo de constantes da funcionalidade, e a troca é realizada a partir de um UIAlertController.
    
<br>   
<img src="screenshots/screenshot-05.png" width="300">

### Tratamento de erros

Tanto na tela de tweets quanto na tela de sentimento, caso ocorra um problema de API ou a mesma não retorne dados, é exibida tela de erro com possibilidade de realizar a requisição novamente.
    
<br>   
<img src="screenshots/screenshot-07.png" width="300"><img src="screenshots/screenshot-06.png" width="300">

