//
//  NetworkingProviderTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import XCTest
@testable import TweetFeeling
@testable import Core

class NetworkingProviderTests: XCTestCase {

    var sut: NetworkingProvider!
    
    override func setUp() {
        super.setUp()
        sut = NetworkingProvider()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func test_downloadFromInvalidUrl_shouldReturnNil() {
        let e = expectation(description: "request")
        let request = RequestMock()
        sut.perform(request) { (response: GoogleErrorModel?) in
            XCTAssertNotNil(response)
            e.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
        
}

extension NetworkingProviderTests {
    
    class GoogleErrorModel: Codable {
        let error: ErrorModel
        struct ErrorModel: Codable {
            let code: Int
            let message: String
            let status: String
        }
    }
    
    class RequestMock: NetworkingRequestProtocol {
        var headers: [String : String]?
        var method: NetworkingMethod { .post }
        var url: String { "https://language.googleapis.com/v1/documents:analyzeSentiment" }
    }
    
}

