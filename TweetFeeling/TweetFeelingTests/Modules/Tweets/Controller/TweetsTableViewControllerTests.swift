//
//  TweetsTableViewControllerTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
import Core
@testable import TweetFeeling

class TweetsTableViewControllerTests: XCTestCase {

    var coordinator: TweetsCoordinatorMock!
    var sut: TweetsTableViewController!
    var viewModel: TweetsViewModelMock!
    
    override func setUp() {
        super.setUp()
        coordinator = TweetsCoordinatorMock()
        viewModel = TweetsViewModelMock()
        sut = TweetsTableViewController(withViewModel: viewModel)
        sut.coordinator = coordinator
    }
    
    override func tearDown() {
        super.tearDown()
        viewModel = nil
        sut = nil
    }
    
    func test_InitWithCoder_isNil() {
        let vc = TweetsTableViewController(coder: NSCoder())
        XCTAssertNil(vc)
    }
    
    func test_tableView_hasTweetsDataProvider() {
        sut.loadViewIfNeeded()
        XCTAssertTrue(sut.tableView.delegate is TweetsDataProvider)
        XCTAssertTrue(sut.tableView.dataSource is TweetsDataProvider)
    }
    
    func test_fetchingData_shouldReturnError() {
        viewModel.returning = nil
        sut.loadViewIfNeeded()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_fetchingData_shouldReturnErrorWithNoData() {
        viewModel.returning = []
        sut.loadViewIfNeeded()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_fetchingData_shouldReturnSuccess() {
        viewModel.returning = [TweetsTestHelper.tweet, TweetsTestHelper.tweet]
        sut.loadViewIfNeeded()
        
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getTweets()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertTrue(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }

    func test_fetchingDataForPagination_shouldReturnError() {
        viewModel.returning = [TweetsTestHelper.tweet, TweetsTestHelper.tweet]
        sut.loadViewIfNeeded()
        
        viewModel.returning = nil
        sut.shouldGetMoreRows()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_fetchingDataForPagination_shouldReturnSuccessWithNoData() {
        viewModel.returning = [TweetsTestHelper.tweet, TweetsTestHelper.tweet]
        sut.loadViewIfNeeded()
        
        viewModel.returning = []
        sut.shouldGetMoreRows()
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getTweets()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertFalse(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_fetchingDataForPagination_shouldReturnSuccess() {
        viewModel.returning = [TweetsTestHelper.tweet]
        sut.loadViewIfNeeded()
        
        viewModel.returning = [TweetsTestHelper.tweet]
        sut.shouldGetMoreRows()
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getTweets()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertTrue(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_fetchingData_shouldReturnSuccessAfterError() {
        viewModel.returning = nil
        sut.loadViewIfNeeded()
        let error = sut.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
        viewModel.returning = [TweetsTestHelper.tweet]
        sut.didTryAgain()
        let tableViewRows = sut.tableView.numberOfRows(inSection: 0)
        let viewModelRows = (viewModel.getTweets()?.count ?? 0) + viewModel.paginationRows()
        XCTAssertTrue(viewModel.canLoadNextPage)
        XCTAssertEqual(tableViewRows, viewModelRows)
    }
    
    func test_tweetSelection_shouldAccessSentiment() {
        sut.loadViewIfNeeded()
        XCTAssertFalse(coordinator.didAccessSentiment)
        sut.didSelect(item: TweetsTestHelper.tweet)
        XCTAssertTrue(coordinator.didAccessSentiment)
    }
    
    func test_usersButtomPressed_shouldAccessUsers() {
        sut.loadViewIfNeeded()
        XCTAssertFalse(coordinator.didAccessUsers)
        guard let button = sut.navigationItem.leftBarButtonItem else {
            XCTFail("não localizou botão de usuários")
            return
        }
        _ = button.target?.perform(button.action)
        XCTAssertTrue(coordinator.didAccessUsers)
    }
    
    func test_didSelectUser_changedCurrentUser() {
        sut.loadViewIfNeeded()
        XCTAssertEqual(viewModel.getUser().id, "1")
        sut.didSelect(user: UserModel(id: "2", name: "test 2", username: "test2"))
        XCTAssertEqual(viewModel.getUser().id, "2")
    }
    
}

extension TweetsTableViewControllerTests {
    
    class TweetsCoordinatorMock: TweetsCoordinatorProtocol {
        
        var didAccessSentiment: Bool = false
        var didAccessUsers: Bool = false
        
        func sentiment(ofTweet tweet: TweetModel) {
            didAccessSentiment = true
        }
        
        func users() {
            didAccessUsers = true
        }
    }
    
    class TweetsViewModelMock: TweetsViewModelProtocol {
        
        var canLoadNextPage: Bool = false
        
        weak var delegate: TweetsViewModelDelegate?
        
        var user: UserModel
        var tweets: [TweetModel]?
        var returning: [TweetModel]?
        
        func set(user: UserModel) {
            self.user = user
        }
        
        init(withUser user: UserModel) {
            self.user = user
        }
        
        required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider()) {
            user = UserModel(id: "1", name: "Test", username: "test")
        }
        
        func getTweets() -> [TweetModel]? {
            return tweets
        }
        func getUser() -> UserModel {
            return user
        }
        
        func fetch() {
            guard let returning = returning else {
                canLoadNextPage = true
                delegate?.didGetError(withMessage: "Teste")
                return
            }
            guard !returning.isEmpty else {
                canLoadNextPage = false
                guard tweets == nil else {
                    delegate?.didGetData()
                    return
                }
                delegate?.didGetError(withMessage: "Teste")
                return
            }
            canLoadNextPage = true
            tweets = tweets ?? []
            tweets?.append(contentsOf: returning)
            delegate?.didGetData()
        }
        
        func nextPage() {
            fetch()
        }
        
        func paginationRows() -> Int {
            guard canLoadNextPage else { return 0 }
            return 1
        }
        
    }
}
