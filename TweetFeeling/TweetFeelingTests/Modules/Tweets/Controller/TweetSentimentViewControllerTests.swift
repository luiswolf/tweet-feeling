//
//  TweetSentimentViewControllerTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
import Core
@testable import TweetFeeling

class TweetSentimentViewControllerTests: XCTestCase {

    func test_initWithCoder_isNil() {
        let cell = TweetSentimentViewController(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_initWithViewModel_shouldGetError() {
        let viewModel = TweetSentimentViewModelMock(withProvider: NetworkingProvider(), andTweet: TweetsTestHelper.tweet)
        let vc = TweetSentimentViewController(withViewModel: viewModel)
        viewModel.sentiment = nil
        vc.loadViewIfNeeded()
        
        let error = vc.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
    }
    
    func test_initWithViewModel_shouldGetData() {
        let viewModel = TweetSentimentViewModelMock(withProvider: NetworkingProvider(), andTweet: TweetsTestHelper.tweet)
        let vc = TweetSentimentViewController(withViewModel: viewModel)
        viewModel.sentiment = TweetSentimentModel(withScore: 0)
        vc.loadViewIfNeeded()
        
        let sentimentView = vc.view as? TweetSentimentView
        
        XCTAssertEqual(sentimentView?.titleLabel.text, viewModel.sentiment?.getSentiment().title)
        XCTAssertEqual(sentimentView?.sentimentLabel.text, viewModel.sentiment?.getSentiment().emoji)
    }
    
    func test_initWithViewModel_shouldGetSuccessAfterError() {
        let viewModel = TweetSentimentViewModelMock(withProvider: NetworkingProvider(), andTweet: TweetsTestHelper.tweet)
        let vc = TweetSentimentViewController(withViewModel: viewModel)
        viewModel.sentiment = nil
        vc.loadViewIfNeeded()
        let error = vc.view.subviews.first(where: {$0 is ContainerView<ErrorView>})
        XCTAssertNotNil(error)
        
        viewModel.sentiment = TweetSentimentModel(withScore: 0)
        vc.didTryAgain()
        let sentimentView = vc.view as? TweetSentimentView
        XCTAssertEqual(sentimentView?.titleLabel.text, viewModel.sentiment?.getSentiment().title)
        XCTAssertEqual(sentimentView?.sentimentLabel.text, viewModel.sentiment?.getSentiment().emoji)
    }
    
}

extension TweetSentimentViewControllerTests {
    
    class TweetSentimentViewModelMock: TweetSentimentViewModelProtocol {
        var tweet: TweetModel
        var sentiment: TweetSentimentModel?
        required init(withProvider provider: NetworkingProviderProtocol, andTweet tweet: TweetModel) {
            self.tweet = tweet
        }
        var delegate: TweetsViewModelDelegate?
        func fetch() {
            guard sentiment != nil else {
                delegate?.didGetError(withMessage: "Erro")
                return
            }
            delegate?.didGetData()
        }
        func getSentiment() -> TweetSentimentProtocol? {
            return sentiment?.getSentiment()
        }
        func getTweet() -> TweetModel {
            tweet
        }
    }
        
}
