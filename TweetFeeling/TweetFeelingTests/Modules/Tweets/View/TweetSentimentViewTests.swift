//
//  TweetSentimentViewTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling

class TweetSentimentViewTests: XCTestCase {

    func test_initWithCoder_isNil() {
        let cell = TweetSentimentView(coder: NSCoder())
        XCTAssertNil(cell)
    }
    
    func test_initWithTweet() {
        let cell = TweetSentimentView(withTweet: TweetsTestHelper.tweet)
        XCTAssertNotNil(cell)
    }
    
    func test_setSentiment_isChandingEmojiAndTitle() {
        let cell = TweetSentimentView(withTweet: TweetsTestHelper.tweet)
        
        let sentiment = HappySentiment()
        cell.set(sentiment: sentiment)
        
        XCTAssertEqual(sentiment.title, cell.titleLabel.text)
        XCTAssertEqual(sentiment.emoji, cell.sentimentLabel.text)
    }

}
