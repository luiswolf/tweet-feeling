//
//  TweetTableViewCellTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling

class TweetTableViewCellTests: XCTestCase {

    func test_initWithCoder_isNil() {
        let cell = TweetTableViewCell(coder: NSCoder())
        XCTAssertNil(cell)
    }

}
