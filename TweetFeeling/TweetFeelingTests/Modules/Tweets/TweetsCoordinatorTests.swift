//
//  TweetsCoordinatorTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import XCTest
@testable import TweetFeeling

class TweetsCoordinatorTests: XCTestCase {

    var sut: TweetsCoordinator!
    var nc: UINavigationController!
    
    override func setUp() {
        super.setUp()
        nc = UINavigationController()
        sut = TweetsCoordinator(navigationController: nc)
    }
    
    override func tearDown() {
        super.tearDown()
        nc = nil
        sut = nil
    }
    
    func test_beforeStart_shouldHaveNoViewControllers() {
        XCTAssertEqual(nc.viewControllers.count, 0)
    }
    
    func test_afterStart_tweetsTableViewControllerIsTopViewController() {
        sut.start()
        XCTAssertTrue(nc.topViewController is TweetsTableViewController)
    }
    
    func test_onSentiment_tweetSentimentTableViewControllerIsTopViewController() {
        
        sut.sentiment(ofTweet: TweetsTestHelper.tweet)
        XCTAssertTrue(nc.topViewController is TweetSentimentViewController)
    }
    
    func test_onUsers_usersAlertControllerIsTopViewController() {
        sut.start()
        XCTAssertTrue(nc.topViewController is TweetsTableViewController)
        sut.users()
        
        let exp = expectation(description: "wait")
        let result = XCTWaiter.wait(for: [exp], timeout: 1.5)
        if result == XCTWaiter.Result.timedOut {
            XCTAssertNotNil(nc.visibleViewController is UsersAlertController)
        } else {
            XCTFail("error")
        }
        
    }
    
}
