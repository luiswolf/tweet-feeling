//
//  TweetsDataProviderTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling
@testable import Core

class TweetsDataProviderTests: XCTestCase {

    var dataProvider: TweetsDataProvider!
    var tableView: UITableView!
    var selectionDelegate: TweetsDataProviderDelegateMock!
    
    override func setUp() {
        super.setUp()
        
        tableView = UITableView()
        selectionDelegate = TweetsDataProviderDelegateMock()
        dataProvider = TweetsDataProvider(withTableView: tableView)
        dataProvider.delegate = selectionDelegate
    }
    
    override func tearDown() {
        super.tearDown()
        
        tableView = nil
        selectionDelegate = nil
        dataProvider = nil
    }
    
    func test_numberOfRows_isZeroAtStart() {
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 0)
    }
    
    func test_numberOfRows_isTweetCountWhenPaginationIsZero() {
        dataProvider.set(items: [TweetsTestHelper.tweet, TweetsTestHelper.tweet], andPaginationRows: 0)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 2)
    }
    
    func test_numberOfRows_isTweetCountWhenPaginationIsNotZero() {
        dataProvider.set(items: [TweetsTestHelper.tweet, TweetsTestHelper.tweet], andPaginationRows: 1)
        XCTAssertEqual(dataProvider.tableView(tableView, numberOfRowsInSection: 0), 3)
    }
    
    func test_titleOfSectionHeader_isBlankSpace() {
        XCTAssertEqual(dataProvider.tableView(tableView, titleForHeaderInSection: 0), " ")
    }
    
    func test_heightForRow_isAutomaticDimension() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForRowAt: IndexPath(row: 0, section: 0)), UITableView.automaticDimension)
    }
    
    func test_heightForHeaderInSection_isEight() {
        XCTAssertEqual(dataProvider.tableView(tableView, heightForHeaderInSection: 0), 8.0)
    }
    
    func test_cell_isNotSelectedAtStart() {
        dataProvider.set(items: [TweetsTestHelper.tweet, TweetsTestHelper.tweet], andPaginationRows: 1)
        XCTAssertEqual(selectionDelegate.didSelectCell, false)
    }
    
    func test_cell_isNotSelectedOnActionIfTheresNoTweetsSet() {
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.didSelectCell, false)
    }
    
    func test_cell_isSelectedOnAction() {
        dataProvider.set(items: [TweetsTestHelper.tweet, TweetsTestHelper.tweet], andPaginationRows: 1)
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.didSelectCell, true)
    }
    
    func test_selectionIssue_hasRightId() {
        let items = [TweetsTestHelper.tweet, TweetsTestHelper.tweet]
        dataProvider.set(items: items, andPaginationRows: 1)
        dataProvider.tableView(tableView, didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.selectedItem?.id, items.first?.id)
    }
    
    func test_defaultCelatasLoaded_whenTheresNoDta() {
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertFalse(cell is TweetTableViewCell)
        XCTAssertFalse(cell is LoaderTableViewCell)
    }
    
    func test_tweetCell_isLoaded() {
        dataProvider.set(items: [TweetsTestHelper.tweet, TweetsTestHelper.tweet], andPaginationRows: 0)
        
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertTrue(cell is TweetTableViewCell)
    }
    
    func test_loaderCell_isLoaded() {
        dataProvider.set(items: [TweetsTestHelper.tweet], andPaginationRows: 1)
        
        let cell = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertTrue(cell is LoaderTableViewCell)
    }
    
    func test_paginationMethod_isNotCalled() {
        dataProvider.set(items: [TweetsTestHelper.tweet], andPaginationRows: 0)
        
        _ = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
        XCTAssertEqual(selectionDelegate.shouldGetMoreRowsValue, false)
    }
    
    func test_paginationMethod_isCalled() {
        dataProvider.set(items: [TweetsTestHelper.tweet], andPaginationRows: 1)
        
        _ = dataProvider.tableView(tableView, cellForRowAt: IndexPath(row: 1, section: 0))
        XCTAssertEqual(selectionDelegate.shouldGetMoreRowsValue, true)
    }

}

extension TweetsDataProviderTests {

    class TweetsDataProviderDelegateMock: TweetsDataProviderDelegate {
        
        var didSelectCell: Bool = false
        var shouldGetMoreRowsValue: Bool = false
        var selectedItem: TweetModel?
        
        func didSelect(item: TweetModel) {
            didSelectCell = true
            selectedItem = item
        }
        
        func shouldGetMoreRows() {
            shouldGetMoreRowsValue = true
        }
    }
    
}

