//
//  TweetSentimentViewModelTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
import Core
@testable import TweetFeeling

class TweetSentimentViewModelTests: XCTestCase {

    let tweet = TweetsTestHelper.tweet
    
    func test_init_shouldHaveSameObject() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = TweetSentimentViewModel(withProvider: provider, andTweet: tweet)
        
        XCTAssertEqual(tweet.id, sut.getTweet().id)
    }
    
    func test_whenObjectIsNil_defaultErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = TweetSentimentViewModel(withProvider: provider, andTweet: tweet)
        
        let delegate = TweetsViewModelDelegateMock()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, TweetsConstant.Message.defaultError)
    }
    
    func test_whenObjectIsSet_dataIsLoaded() {
        let sentiment = TweetSentimentModel(withScore: 0)
        let provider = MockNetworkingProvider(withModel: sentiment)
        let sut = TweetSentimentViewModel(withProvider: provider, andTweet: tweet)
        let delegate = TweetsViewModelDelegateMock()
        sut.delegate = delegate
        sut.fetch()

        XCTAssertEqual(delegate.gotData, true)
        XCTAssertEqual(sut.getSentiment()?.title, sentiment.getSentiment().title)
    }

}

// MARK: - Helper
extension TweetSentimentViewModelTests {
    
    class TweetsViewModelDelegateMock: TweetsViewModelDelegate {

        var gotData: Bool = false
        var error: String?
        
        func didGetData() {
            gotData = true
        }
        
        func didGetError(withMessage message: String) {
            error = message
        }
    }
        
    class MockNetworkingProvider: NetworkingProviderProtocol {
        
        var performCount: Int = 0
        var model: TweetSentimentModel?
        
        init(withModel model: TweetSentimentModel?) {
            self.model = model
        }
        
        func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
            performCount += 1
            callback(model as? T)
        }
        
    }

}
