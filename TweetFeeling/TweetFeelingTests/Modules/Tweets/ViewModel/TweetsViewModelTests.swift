//
//  TweetsViewModelTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
import Core
@testable import TweetFeeling

class TweetsViewModelTests: XCTestCase {

    let tweet = TweetsTestHelper.tweet
    
    func test_whenObjectIsNil_defaultErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: nil)
        let sut = TweetsViewModel(withProvider: provider)
        
        let delegate = TweetsViewModelDelegateMock()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 1)
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, TweetsConstant.Message.defaultError)
    }
    
    func test_whenObjectIsEmpty_noDataErrorIsReturned() {
        let provider = MockNetworkingProvider(withModel: [TweetModel]())
        let sut = TweetsViewModel(withProvider: provider)
        let delegate = TweetsViewModelDelegateMock()
        sut.delegate = delegate
        sut.fetch()
        XCTAssertEqual(sut.paginationRows(), 0)
        XCTAssertEqual(delegate.gotData, false)
        XCTAssertEqual(delegate.error, TweetsConstant.Message.noDataError)
    }

    func test_whenObjectIsSet_nextRequestGotNoData() {
        let provider = MockNetworkingProvider(withModel: [tweet])
        let sut = TweetsViewModel(withProvider: provider)
        let delegate = TweetsViewModelDelegateMock()
        sut.delegate = delegate
        sut.fetch()
        provider.empty()
        sut.fetch()

        XCTAssertEqual(sut.paginationRows(), 0)
        XCTAssertEqual(delegate.gotData, true)
        XCTAssertEqual(sut.getTweets()?.count, 1)
    }

    func test_whenObjectIsSet_dataIsLoaded() {
        let provider = MockNetworkingProvider(withModel: [tweet])
        let sut = TweetsViewModel(withProvider: provider)
        let delegate = TweetsViewModelDelegateMock()
        sut.delegate = delegate
        sut.fetch()

        XCTAssertEqual(sut.paginationRows(), 1)
        XCTAssertEqual(delegate.gotData, true)
        XCTAssertEqual(sut.getTweets()?.count, 1)
    }
    
    func test_setUser_chantViewModelUser() {
        let provider = MockNetworkingProvider(withModel: [tweet])
        let sut = TweetsViewModel(withProvider: provider)
        let user = UserModel(id: "123", name: "Test", username: "test")
        sut.set(user: user)
        XCTAssertEqual(user.name, sut.getUser().name)
        XCTAssertEqual(user.username, sut.getUser().username)
        XCTAssertEqual(user.id, sut.getUser().id)
    }

}

// MARK: - Helper
extension TweetsViewModelTests {
    
    class TweetsViewModelDelegateMock: TweetsViewModelDelegate {

        var gotData: Bool = false
        var error: String?
        
        func didGetData() {
            gotData = true
        }
        
        func didGetError(withMessage message: String) {
            error = message
        }
    }
        
    class MockNetworkingProvider: NetworkingProviderProtocol {
        
        var performCount: Int = 0
        var model: [TweetModel]?
        
        init(withModel model: [TweetModel]?) {
            self.model = model
        }
        
        func empty() {
            model = []
        }
        
        func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
            performCount += 1
            guard let model = model else {
                callback(nil)
                return
            }
            let response = WrapperModel<TweetModel>(data: model, meta: WrapperModel.Meta(nextToken: nil))
            callback(response as? T)
        }
        
    }

}
