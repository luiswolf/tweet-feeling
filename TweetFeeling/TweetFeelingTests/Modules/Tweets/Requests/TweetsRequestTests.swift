//
//  TweetsRequestTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import XCTest
@testable import TweetFeeling

class TweetsRequestTests: XCTestCase {

    var sut: TweetsRequest!
//    let tweet = TweetsTestHelper.tweet
    
    override func setUp() {
        super.setUp()
        
        sut = TweetsRequest(withUserId: "1")
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
    }
    
    func test_init_isSettingObject() {
        XCTAssertEqual(sut.userId, "1")
        XCTAssertEqual(sut.method, .get)
    }
    
    func test_setUserId_isWorkingAndResetingPaginationToken() {
        sut.set(userId: "2")
        XCTAssertEqual(sut.userId, "2")
        XCTAssertNil(sut.paginationToken)
    }
    
    func test_setUrl_whenTokenIsNil() {
        let url = TweetsConstant.Endpoint.tweets
            .replacingOccurrences(of: "{user-id}", with: "1")
        XCTAssertEqual(sut.url, url)
    }
    
    func test_setUrl_whenTokenIsDefined() {
        let url = TweetsConstant.Endpoint.tweets
            .replacingOccurrences(of: "{user-id}", with: "1")
            .appending("&pagination_token=ABC")
        sut.paginationToken = "ABC"
        XCTAssertEqual(sut.url, url)
    }
    
}
