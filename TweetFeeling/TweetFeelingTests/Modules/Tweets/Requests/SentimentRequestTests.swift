//
//  SentimentRequestTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import XCTest
@testable import TweetFeeling

class SentimentRequestTests: XCTestCase {

    var sut: SentimentRequest!
    let tweet = TweetsTestHelper.tweet
    
    override func setUp() {
        super.setUp()
        
        sut = SentimentRequest(withTweet: tweet)
    }
    
    override func tearDown() {
        super.tearDown()
        
        sut = nil
    }
    
    func test_init_isSettingObject() {
        XCTAssertEqual(sut.text, tweet.text)
        XCTAssertEqual(sut.method, .post)
        XCTAssertEqual(sut.url, TweetsConstant.Endpoint.sentiment)
        
        let expectedText: String? = {
            guard let document = sut.parameters?["document"] as? [String : Any] else {
                return nil
            }
            return document["content"] as? String
        }()
        XCTAssertEqual(expectedText, tweet.text)
    }
    
}
