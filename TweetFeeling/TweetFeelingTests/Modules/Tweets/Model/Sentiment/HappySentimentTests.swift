//
//  HappySentimentTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling

class HappySentimentTests: XCTestCase {
    
    var sut: HappySentiment!
    
    override func setUp() {
        super.setUp()
        sut = HappySentiment()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func test_values() {
        XCTAssertEqual(sut.title, TweetsConstant.Title.happy)
        XCTAssertEqual(sut.emoji, TweetsConstant.Emoji.happy)
    }
}
