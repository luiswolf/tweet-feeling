//
//  SadSentimentTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling

class SadSentimentTests: XCTestCase {
    
    var sut: SadSentiment!
    
    override func setUp() {
        super.setUp()
        sut = SadSentiment()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func test_values() {
        XCTAssertEqual(sut.title, TweetsConstant.Title.sad)
        XCTAssertEqual(sut.emoji, TweetsConstant.Emoji.sad)
    }
}
