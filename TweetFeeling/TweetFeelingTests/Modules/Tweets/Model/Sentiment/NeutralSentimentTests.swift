//
//  NeutralSentimentTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling

class NeutralSentimentTests: XCTestCase {
    
    var sut: NeutralSentiment!
    
    override func setUp() {
        super.setUp()
        sut = NeutralSentiment()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func test_values() {
        XCTAssertEqual(sut.title, TweetsConstant.Title.neutral)
        XCTAssertEqual(sut.emoji, TweetsConstant.Emoji.neutral)
    }
}
