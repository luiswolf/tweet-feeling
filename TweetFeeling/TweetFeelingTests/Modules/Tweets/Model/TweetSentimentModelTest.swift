//
//  TweetSentimentModelTest.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import XCTest
@testable import TweetFeeling

class TweetSentimentModelTest: XCTestCase {
    
    func test_init_forSadSentiment() {
        let sut = TweetSentimentModel(withScore: -1)
        XCTAssertTrue(sut.getSentiment() is SadSentiment)
    }
    
    func test_init_forNeutralSentiment() {
        let sut = TweetSentimentModel(withScore: 0)
        XCTAssertTrue(sut.getSentiment() is NeutralSentiment)
    }
    
    func test_init_forHappySentiment() {
        let sut = TweetSentimentModel(withScore: 1)
        XCTAssertTrue(sut.getSentiment() is HappySentiment)
    }
    
}
