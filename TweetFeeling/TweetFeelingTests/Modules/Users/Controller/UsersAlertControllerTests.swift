//
//  UsersAlertControllerTests.swift
//  TweetFeelingTests
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import XCTest
import Core
@testable import TweetFeeling

class UsersAlertControllerTests: XCTestCase {

    var sut: UsersAlertController!
    var delegate: UserSelectionDelegateMock!
    
    override func setUp() {
        super.setUp()
        delegate = UserSelectionDelegateMock()
        sut = UsersAlertController()
        sut.usersDelegate = delegate
    }
    
    override func tearDown() {
        super.tearDown()
        delegate = nil
        sut = nil
    }
    
    func test_alertController_isLoaded() {
        sut.loadViewIfNeeded()
        
        XCTAssertEqual(sut.title, "Alterar usuário")
        XCTAssertEqual(sut.actions.count, UsersConstant.profiles.count + 1)
    }
    
}

extension UsersAlertControllerTests {
    
    class UserSelectionDelegateMock: UserSelectionDelegate {
        var itemSelected: Bool = false
        func didSelect(user: UserModel) {
            itemSelected = true
        }
    }
}


