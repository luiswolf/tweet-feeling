//
//  TweetsRequest.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation
import Core

struct TweetsRequest: NetworkingRequestProtocol {
    
    var userId: UserModel.Id
    
    var paginationToken: String?
    
    var method: NetworkingMethod { .get }
    
    var url: String {
        let url = TweetsConstant.Endpoint.tweets
            .replacingOccurrences(of: "{user-id}", with: userId)
        guard let paginationToken = paginationToken else { return url }
        return url.appending("&pagination_token=\(paginationToken)")
    }
    
    var headers: [String : String]? {
        ["Authorization": Configuration.twitterToken]
    }
    
    init(withUserId userId: UserModel.Id) {
        self.userId = userId
    }
    
}

// MARK: - Helper
extension TweetsRequest {
    
    mutating func set(userId: UserModel.Id) {
        self.userId = userId
        self.paginationToken = nil
    }
    
    mutating func set(paginationToken: String?) {
        self.paginationToken = paginationToken
    }
    
}
