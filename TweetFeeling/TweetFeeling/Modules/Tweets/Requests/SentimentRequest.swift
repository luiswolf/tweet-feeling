//
//  SentimentRequest.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation
import Core

struct SentimentRequest: NetworkingRequestProtocol {
    
    var method: NetworkingMethod { .post }
    
    var url: String { TweetsConstant.Endpoint.sentiment }
    
    var parameters: [String : Any]? {
        return [
            "encodingType": "UTF8",
            "document": [
                "type": "PLAIN_TEXT",
                "content": text
            ]
        ]
    }
    
    var text: String
    
    init(withTweet tweet: TweetModel) {
        self.text = tweet.text
    }
    
}

