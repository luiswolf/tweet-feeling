//
//  TweetsCoordinatorProtocol.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 21/03/21.
//

import Foundation


protocol TweetsCoordinatorProtocol: class {
    
    func sentiment(ofTweet tweet: TweetModel)
    
    func users()
    
}
