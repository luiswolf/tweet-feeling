//
//  TweetsViewModelDelegate.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

protocol TweetsViewModelDelegate: class {
    
    func didGetData()
    
    func didGetError(withMessage message: String)
    
}
