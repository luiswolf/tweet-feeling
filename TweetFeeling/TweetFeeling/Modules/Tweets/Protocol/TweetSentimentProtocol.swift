//
//  TweetSentimentProtocol.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation

protocol TweetSentimentProtocol {
    
    var title: String { get }
    
    var emoji: String { get }
    
}
