//
//  TweetsDataProviderDelegate.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

protocol TweetsDataProviderDelegate: class {
    
    func didSelect(item: TweetModel)
    
    func shouldGetMoreRows()
    
}
