//
//  TweetsViewModelProtocol.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation
import Core

protocol TweetsViewModelProtocol {
    
    var delegate: TweetsViewModelDelegate? { get set }
    
    init(withProvider provider: NetworkingProviderProtocol)
    
    func getUser() -> UserModel
    
    func set(user: UserModel)
    
    func getTweets() -> [TweetModel]?
    
    func fetch()
    
    func paginationRows() -> Int

}
