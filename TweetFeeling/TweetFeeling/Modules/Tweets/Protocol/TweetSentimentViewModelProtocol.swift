//
//  TweetSentimentViewModelProtocol.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import Foundation
import Core

protocol TweetSentimentViewModelProtocol {
    
    func getTweet() -> TweetModel
    
    func getSentiment() -> TweetSentimentProtocol?
    
    var delegate: TweetsViewModelDelegate? { get set }
    
    init(withProvider provider: NetworkingProviderProtocol, andTweet tweet: TweetModel)
    
    func fetch()
    
}
