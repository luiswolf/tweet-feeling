//
//  NeutralSentiment.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation

struct NeutralSentiment: TweetSentimentProtocol {
    
    private typealias Constant = TweetsConstant
    
    var title: String { Constant.Title.neutral }
    
    var emoji: String { Constant.Emoji.neutral }
    
}
