//
//  HappySentiment.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation

struct HappySentiment: TweetSentimentProtocol {
    
    private typealias Constant = TweetsConstant
    
    var title: String { Constant.Title.happy }
    
    var emoji: String { Constant.Emoji.happy }
    
}
