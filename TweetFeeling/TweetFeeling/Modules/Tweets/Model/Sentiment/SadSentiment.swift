//
//  SadSentiment.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation

struct SadSentiment: TweetSentimentProtocol {
    
    private typealias Constant = TweetsConstant
    
    var title: String { Constant.Title.sad }
    
    var emoji: String { Constant.Emoji.sad }
    
}
