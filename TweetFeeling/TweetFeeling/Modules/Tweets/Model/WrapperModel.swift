//
//  WrapperModel.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

struct WrapperModel<T: Codable>: Codable {
    
    struct Meta: Codable {
        let nextToken: String?
        private enum CodingKeys: String, CodingKey {
            case nextToken = "next_token"
        }
    }
    
    let data: [T]?
    let meta: Meta
    
}
