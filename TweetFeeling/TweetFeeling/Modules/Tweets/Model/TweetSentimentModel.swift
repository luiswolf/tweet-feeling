//
//  TweetSentimentModel.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation

struct TweetSentimentModel: Codable {
    
    struct DocumentSentiment: Codable {
        let score: Double
    }

    private let documentSentiment: DocumentSentiment
    
}

// MARK: - Helper
extension TweetSentimentModel {
    
    init(withScore score: Double) {
        documentSentiment = DocumentSentiment(score: score)
    }
    
    func getSentiment() -> TweetSentimentProtocol {
        if documentSentiment.score < -0.25 {
            return SadSentiment()
        }
        if documentSentiment.score > 0.25 {
            return HappySentiment()
        }
        return NeutralSentiment()
    }
    
}
