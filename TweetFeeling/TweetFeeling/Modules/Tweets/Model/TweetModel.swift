//
//  TweetModel.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

struct TweetModel: Codable {
    
    let id: String
    let date: Date
    let text: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case date = "created_at"
        case text
    }
    
}
