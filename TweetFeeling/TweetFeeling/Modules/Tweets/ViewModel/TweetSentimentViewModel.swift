//
//  TweetSentimentViewModel.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import Foundation
import Core

class TweetSentimentViewModel: NSObject, TweetSentimentViewModelProtocol {
    
    private typealias Constant = TweetsConstant
    
    weak var delegate: TweetsViewModelDelegate?
    
    private var tweet: TweetModel
    private var sentiment: TweetSentimentProtocol?
    
    private let neworkingProvider: NetworkingProviderProtocol
    
    private lazy var request: SentimentRequest = {
        let request = SentimentRequest(withTweet: tweet)
        return request
    }()
    
    required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider(), andTweet tweet: TweetModel) {
        self.neworkingProvider = provider
        self.tweet = tweet
    }
    
    func getTweet() -> TweetModel { tweet }
    
    func getSentiment() -> TweetSentimentProtocol? { sentiment }
    
}

// MARK: - Networking
extension TweetSentimentViewModel {
    
    func fetch() {
        neworkingProvider.perform(request) { [weak self] (response: TweetSentimentModel?) in
            
            guard let response = response else {
                self?.delegate?.didGetError(withMessage: Constant.Message.defaultError)
                return
            }

            self?.sentiment = response.getSentiment()
            self?.delegate?.didGetData()
        }
    }
    
}
