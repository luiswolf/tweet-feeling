//
//  TweetsViewModel.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation
import Core

class TweetsViewModel: NSObject, TweetsViewModelProtocol {
    
    private typealias Constant = TweetsConstant
    
    weak var delegate: TweetsViewModelDelegate?
    
    private var canLoadNextPage: Bool = false
    private var tweets: [TweetModel]?
    private var user: UserModel = UsersConstant.Profile.twitterDev {
        didSet {
            tweets = nil
            delegate?.didGetData()
        }
    }
    
    private let neworkingProvider: NetworkingProviderProtocol
    private lazy var request: TweetsRequest = {
        let request = TweetsRequest(withUserId: user.id)
        return request
    }()
    
    required init(withProvider provider: NetworkingProviderProtocol = NetworkingProvider()) {
        self.neworkingProvider = provider
    }
    
    func set(user: UserModel) {
        self.user = user
        request.set(userId: user.id)
    }
    
    func getUser() -> UserModel { user }
    
    func getTweets() -> [TweetModel]? { tweets }
    
}

// MARK: - Networking
extension TweetsViewModel {
    
    func fetch() {
        neworkingProvider.perform(request) { [weak self] (response: WrapperModel<TweetModel>?) in
            
            guard let response = response else {
                self?.canLoadNextPage = true
                self?.delegate?.didGetError(withMessage: Constant.Message.defaultError)
                return
            }

            guard let data = response.data, !data.isEmpty else {
                self?.canLoadNextPage = false
                guard self?.tweets == nil else {
                    self?.delegate?.didGetData()
                    return
                }
                self?.delegate?.didGetError(withMessage: Constant.Message.noDataError)
                return
            }

            self?.canLoadNextPage = true
            self?.request.set(paginationToken: response.meta.nextToken)
            self?.tweets = self?.tweets ?? []
            self?.tweets?.append(contentsOf: data)
            self?.delegate?.didGetData()
        }
    }
    
}

// MARK: - Pagination
extension TweetsViewModel {
    
    func paginationRows() -> Int {
        guard canLoadNextPage else { return 0 }
        return 1
    }
    
}
