//
//  TweetsCoordinator.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import UIKit
import Core

class TweetsCoordinator: CoordinatorProtocol, TweetsCoordinatorProtocol {
    
    var childCoordinators = [CoordinatorProtocol]()
    
    var navigationController: UINavigationController
    
    private lazy var vc: TweetsTableViewController = {
        let vc = TweetsTableViewController()
        vc.coordinator = self
        return vc
    }()

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.pushViewController(vc, animated: true)
    }
    
}

// MARK: - Actions
extension TweetsCoordinator {
    
    func sentiment(ofTweet tweet: TweetModel) {
        let vm = TweetSentimentViewModel(withProvider: NetworkingProvider(), andTweet: tweet)
        let vc = TweetSentimentViewController(withViewModel: vm)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func users() {
        let ac = UsersAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        ac.usersDelegate = vc
        ac.sourceView = vc.view
        navigationController.present(ac, animated: true, completion: nil)
    }
    
}
