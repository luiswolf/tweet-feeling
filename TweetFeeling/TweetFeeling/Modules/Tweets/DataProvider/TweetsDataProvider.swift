//
//  TweetsDataProvider.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import UIKit
import Core

class TweetsDataProvider: NSObject {
    
    typealias Delegate = TweetsDataProviderDelegate
    typealias ItemModel = TweetModel
    private typealias ItemCell = TweetTableViewCell
    private typealias LoaderCell = LoaderTableViewCell
    
    weak var delegate: Delegate?
    weak var tableView: UITableView?
    
    private var items: [ItemModel]?
    private var paginationRows: Int = 0
    
    func set(items: [ItemModel]?, andPaginationRows paginationRows: Int) {
        self.items = items
        self.paginationRows = paginationRows
    }
    
    private func register() {
        tableView?.register(ItemCell.self, forCellReuseIdentifier: ItemCell.identifier)
        tableView?.register(LoaderCell.self, forCellReuseIdentifier: LoaderCell.identifier)
    }
    
    init(withTableView tableView: UITableView?) {
        super.init()
        self.tableView = tableView
        self.register()
    }
    
}

// MARK: - UITableViewDelegate
extension TweetsDataProvider: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let items = items else { return }
        let item = items[indexPath.row]
        delegate?.didSelect(item: item)
    }
    
}

// MARK: - UITableViewDataSource
extension TweetsDataProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let items = items else { return 0 }
        return items.count + paginationRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let items = items else { return UITableViewCell() }
        guard indexPath.row == items.count else {
            let item = items[indexPath.row]
            return cell(forItem: item, atIndexPath: indexPath)
        }
        delegate?.shouldGetMoreRows()
        return cell(forLoaderAtIndexPath: indexPath)
    }
    
}

// MARK: - Helper
extension TweetsDataProvider {
    
    private func dequeue<T: CellIdentifier>(cellOfType type: T.Type, atIndexPath indexPath: IndexPath) -> T {
        return tableView?.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as! T
    }
    
    private func cell(forItem item: ItemModel,  atIndexPath indexPath: IndexPath) -> ItemCell {
        let cell = dequeue(cellOfType: ItemCell.self, atIndexPath: indexPath)
        cell.configure(withTweet: item)
        return cell
    }

    private func cell(forLoaderAtIndexPath indexPath: IndexPath) -> LoaderCell {
        return dequeue(cellOfType: LoaderCell.self, atIndexPath: indexPath)
    }
    
}
