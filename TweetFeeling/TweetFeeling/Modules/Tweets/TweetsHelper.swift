//
//  TweetsHelper.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import UIKit

class TweetsHelper {
    
    static func format(tweet: String) -> NSAttributedString {
        let range = NSRange(location: 0, length: tweet.count)
        let regex = try! NSRegularExpression(pattern: "[@#][a-zA-Z0-9_]*")
        let matches = regex.matches(in: tweet, options: [], range: range)
        
        let blackFormat: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.text
        ]
        let blueFormat: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: UIColor.blue,
            NSAttributedString.Key.font: UIFont.text
        ]
    
        let final = NSMutableAttributedString(string: tweet, attributes: blackFormat)
        matches.forEach { final.addAttributes(blueFormat, range: $0.range) }
        
        return final
    }
    
}
