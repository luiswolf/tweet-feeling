//
//  TweetSentimentViewController.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import UIKit
import Core

class TweetSentimentViewController: UIViewController, LoaderProtocol, ErrorProtocol, ErrorViewDelegate {
    
    private typealias Constant = TweetsConstant
    
    weak var errorDelegate: ErrorViewDelegate? { self }
    
    private var viewModel: TweetSentimentViewModelProtocol
    
    private lazy var contentView: TweetSentimentView = {
        return TweetSentimentView(withTweet: viewModel.getTweet())
    }()
    
    init(withViewModel viewModel: TweetSentimentViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Lifecycle
extension TweetSentimentViewController {
    
    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        title = Constant.Title.sentiment
        showLoader()
        viewModel.fetch()
    }
    
}

// MARK: - TweetsViewModelDelegate
extension TweetSentimentViewController: TweetsViewModelDelegate {
    
    func didGetData() {
        hideLoader()
        if let sentiment = viewModel.getSentiment() {
            contentView.set(sentiment: sentiment)
        }
    }
    
    func didGetError(withMessage message: String) {
        hideLoader()
        showError(withMessage: message)
    }
    
    func didTryAgain() {
        hideError()
        showLoader()
        viewModel.fetch()
    }
    
}
