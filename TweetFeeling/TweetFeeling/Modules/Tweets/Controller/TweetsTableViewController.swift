//
//  TweetsTableViewController.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import UIKit
import Core

class TweetsTableViewController: UITableViewController, LoaderProtocol, ErrorProtocol, ErrorViewDelegate {

    private typealias Constant = TweetsConstant
    
    weak var coordinator: TweetsCoordinatorProtocol?
    weak var errorDelegate: ErrorViewDelegate? { self }
    
    private var viewModel: TweetsViewModelProtocol
    
    private lazy var dataProvider: TweetsDataProvider = {
        let provider = TweetsDataProvider(withTableView: tableView)
        provider.delegate = self
        return provider
    }()
    
    init(withViewModel viewModel: TweetsViewModelProtocol = TweetsViewModel()) {
        self.viewModel = viewModel
        super.init(style: .grouped)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Lifecycle
extension TweetsTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:  Constant.Image.changeUser), style: .plain, target: self, action: #selector(changeUser))
        updateUser()
        tableView.delegate = dataProvider
        tableView.dataSource = dataProvider
        fetchData()
    }
    
}

// MARK: - TweetsDataProviderDelegate
extension TweetsTableViewController: TweetsDataProviderDelegate {

    func shouldGetMoreRows() {
        viewModel.fetch()
    }

    func didSelect(item: TweetModel) {
        coordinator?.sentiment(ofTweet: item)
    }

}

// MARK: - TweetsViewModelDelegate
extension TweetsTableViewController: TweetsViewModelDelegate {
    
    func didGetData() {
        hideLoader()
        dataProvider.set(items: viewModel.getTweets(), andPaginationRows: viewModel.paginationRows())
        tableView.reloadData()
    }
    
    func didGetError(withMessage message: String) {
        hideLoader()
        showError(withMessage: message)
    }
    
    func didTryAgain() {
        fetchData()
    }
    
}

// MARK: - Actions
extension TweetsTableViewController {
    
    @objc
    private func changeUser() {
        coordinator?.users()
    }
    
}

// MARK: - Helper
extension TweetsTableViewController {
    
    private func updateUser() {
        navigationItem.set(title: viewModel.getUser().name, withSubtitle: viewModel.getUser().fullUsername)
    }
    
    func fetchData() {
        hideError()
        showLoader()
        viewModel.fetch()
    }
    
}

// MARK: - UserSelectionDelegate
extension TweetsTableViewController: UserSelectionDelegate {
    
    func didSelect(user: UserModel) {
        viewModel.set(user: user)
        updateUser()
        fetchData()
    }
    
}
