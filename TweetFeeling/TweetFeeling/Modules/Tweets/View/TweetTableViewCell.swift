//
//  TweetTableViewCell.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import UIKit
import Core

class TweetTableViewCell: UITableViewCell, CellIdentifier {

    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.font = .caption1
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.font = .text
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .fill
        view.distribution = .fill
        view.spacing = 4.0
        view.addArrangedSubview(dateLabel)
        view.addArrangedSubview(messageLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
}

// MARK: - Setup
extension TweetTableViewCell {
    
    private func commonInit() {
        contentView.addSubview(stackView)
        selectedBackgroundView = CellBackgroundView()
        autoLayout()
    }
    
    private func autoLayout() {
        let margin = contentView.layoutMarginsGuide
        let bottomConstraint = stackView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
        bottomConstraint.priority = .fittingSizeLevel
        let constraints = [
            stackView.topAnchor.constraint(equalTo: margin.topAnchor),
            bottomConstraint,
            stackView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: margin.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func configure(withTweet tweet: TweetModel) {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        dateLabel.text = formatter.string(from: tweet.date)
        messageLabel.text = tweet.text
        messageLabel.attributedText = TweetsHelper.format(tweet: tweet.text)
    }
    
}
