//
//  TweetSentimentView.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import UIKit

class TweetSentimentView: UIView {

    private lazy var stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.spacing = 16.0
        view.addArrangedSubview(textStackView)
        view.addArrangedSubview(messageLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var textStackView: UIStackView = {
        let view = UIStackView()
        view.axis = .vertical
        view.alignment = .center
        view.distribution = .fill
        view.spacing = 0
        view.addArrangedSubview(sentimentLabel)
        view.addArrangedSubview(titleLabel)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var messageLabel: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.backgroundColor = UIColor.extraLightGray.withAlphaComponent(0.3)
        textView.textContainerInset = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.extraLightGray.cgColor
        return textView
    }()

    private(set) lazy var sentimentLabel: UILabel = {
        let label = UILabel()
        label.font = .emoji
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .title
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    init(withTweet tweet: TweetModel) {
        super.init(frame: .zero)
        messageLabel.attributedText = TweetsHelper.format(tweet: tweet.text)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        return nil
    }
    
}

// MARK: - Helper
extension TweetSentimentView {
    
    private func commonInit() {
        addSubview(stackView)
        autoLayout()
    }

    private func autoLayout() {
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func set(sentiment: TweetSentimentProtocol) {
        titleLabel.text = sentiment.title
        sentimentLabel.text = sentiment.emoji
    }
    
}
