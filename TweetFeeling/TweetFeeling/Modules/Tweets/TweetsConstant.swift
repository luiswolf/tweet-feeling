//
//  TweetsConstant.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

struct TweetsConstant {
    
    enum Endpoint {
        static let tweets =
            "https://api.twitter.com/2/users/{user-id}/tweets?tweet.fields=created_at&user.fields=created_at&max_results=20"
        static let sentiment = "https://language.googleapis.com/v1/documents:analyzeSentiment?key=\(Configuration.googleApiKey)"
    }
    
    enum Emoji {
        static let happy = "😃"
        static let neutral = "😐"
        static let sad = "☹️"
    }
    
    enum Title {
        static let happy = "Feliz"
        static let neutral = "Neutro"
        static let sad = "Triste"
        static let sentiment = "Sentimento"
    }
    
    enum Image {
        static let changeUser = "change-user"
    }
    
    enum Message {
        static let defaultError = "Não foi possível consultar tweets. Tente novamente mais tarde."
        static let noDataError = "Não há tweets para exibir."
    }
    
}

