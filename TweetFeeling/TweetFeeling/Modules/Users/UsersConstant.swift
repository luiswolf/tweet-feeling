//
//  UsersConstant.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

struct UsersConstant {
    
    static let profiles: [UserModel] = [Profile.apple, Profile.google, Profile.twitterDev, Profile.youtube, Profile.error]
    
    enum Profile {
        static let apple = UserModel(
            id: "380749300",
            name: "Apple",
            username: "Apple"
        )
        static let error = UserModel(
            id: "12345",
            name: "Error",
            username: "erroracount"
        )
        static let google = UserModel(
            id: "20536157",
            name: "Google",
            username: "Google"
        )
        static let twitterDev = UserModel(
            id: "2244994945",
            name: "Twitter Dev",
            username: "TwitterDev"
        )
        static let youtube = UserModel(
            id: "10228272",
            name: "YouTube",
            username: "YouTube"
        )
    }
    
    enum Title {
        static let cancel = "Cancelar"
        static let changeUser = "Alterar usuário"
    }
    
}
