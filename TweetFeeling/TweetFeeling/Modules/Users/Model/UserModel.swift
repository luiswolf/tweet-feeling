//
//  UserModel.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

struct UserModel: Codable {
    
    typealias Id = String
    
    let id: Id
    let name: String
    let username: String
    
}

// MARK: - Helper
extension UserModel {
    
    var fullUsername: String {
        return "@" + username
    }
    
}
