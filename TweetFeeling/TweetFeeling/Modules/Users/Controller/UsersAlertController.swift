//
//  UsersAlertController.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import UIKit

class UsersAlertController: UIAlertController {
    
    private typealias Title = UsersConstant.Title
    weak var usersDelegate: UserSelectionDelegate?
    var sourceView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = Title.changeUser
        getActions().forEach { addAction($0) }
        popoverPresentationController?.sourceView = sourceView
        popoverPresentationController?.sourceRect = CGRect(
            x: sourceView.bounds.midX,
            y: sourceView.bounds.midY,
            width: 0,
            height: 0
        )
        popoverPresentationController?.permittedArrowDirections = []
    }
    
}

// MARK: - Actions
extension UsersAlertController {
    
    private func getActions() -> [UIAlertAction] {
        let completion: (UserModel) -> ((UIAlertAction) -> Void) = { user in
            let callback: (UIAlertAction) -> Void = { _ in
                self.usersDelegate?.didSelect(user: user)
            }
            return callback
        }
        
        
        var _actions = UsersConstant.profiles.map { (user: UserModel) -> UIAlertAction in
            UIAlertAction(
                title: user.name,
                style: .default,
                handler: completion(user)
            )
        }
        _actions.append(UIAlertAction(title: Title.cancel, style: .cancel))
        return _actions
    }
    
}
