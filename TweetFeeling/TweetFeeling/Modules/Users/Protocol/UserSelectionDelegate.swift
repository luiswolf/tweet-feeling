//
//  UserSelectionDelegate.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 20/03/21.
//

import Foundation

protocol UserSelectionDelegate: class {
    
    func didSelect(user: UserModel)
    
}
