//
//  NetworkingProvider.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation
import Core
import Alamofire

class NetworkingProvider: NetworkingProviderProtocol {
    
    func perform<T>(_ request: NetworkingRequestProtocol, returningTo callback: @escaping CodableCallback<T>) where T: Codable {
        
        let headers: HTTPHeaders? = {
            guard let headers = request.headers else { return nil }
            return HTTPHeaders(headers)
        }()
        
        AF.request(
            request.url,
            method: request.method.alamofireMethod,
            parameters: request.parameters,
            encoding: JSONEncoding.default,
            headers: headers
        ).responseDecodable(
            of: T.self,
            decoder: DateDecoder()
        ){ response in
            DispatchQueue.main.async {
                callback(response.value)
            }
        }
        
    }

}
