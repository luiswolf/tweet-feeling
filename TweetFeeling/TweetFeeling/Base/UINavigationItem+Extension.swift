//
//  UINavigationItem+Extension.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 19/03/21.
//

import UIKit

extension UINavigationItem {

     func set(title: String, withSubtitle subtitle: String) {

        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = .title
        titleLabel.textAlignment = .center
        titleLabel.textColor = .blue
        titleLabel.sizeToFit()

        let subtitleLabel = UILabel()
        subtitleLabel.text = subtitle
        subtitleLabel.font = .caption2
        subtitleLabel.textAlignment = .center
        subtitleLabel.textColor = .gray
        subtitleLabel.sizeToFit()

        let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 2

        let width = max(titleLabel.frame.size.width, subtitleLabel.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 37)

        titleLabel.sizeToFit()
        subtitleLabel.sizeToFit()

        titleView = stackView
        
    }
}
