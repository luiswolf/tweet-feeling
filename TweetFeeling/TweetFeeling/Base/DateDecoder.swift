//
//  DateDecoder.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import Foundation

public class DateDecoder: JSONDecoder {
    
    public override init() {
        super.init()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateDecodingStrategy = .formatted(formatter)
    }
    
}
