//
//  AppDelegate.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 17/03/21.
//

import UIKit
import Core

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: CoordinatorProtocol?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        CellBackgroundView.appearance().backgroundColor = UIColor.blue.withAlphaComponent(0.1)
        UINavigationBar.appearance().backgroundColor = .blue
        UINavigationBar.appearance().tintColor = .blue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.blue]
        UITableView.appearance().separatorColor = UIColor.lightGray
        UITableView.appearance().backgroundColor = UIColor.extraLightGray
        
        let nc = UINavigationController()
        coordinator = TweetsCoordinator(navigationController: nc)
        coordinator?.start()

        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = nc
        window.makeKeyAndVisible()
        window.tintColor = .black
        
        self.window = window
        return true
    }

}
