//
//  UIFont+Extension.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import UIKit

extension UIFont {
    
    static let title = UIFont(name: "Avenir-Bold", size: 16.0) ?? .boldSystemFont(ofSize: 16.0)
    static let caption1 = UIFont(name: "Avenir", size: 12.0) ?? .systemFont(ofSize: 12.0)
    static let caption2 = UIFont(name: "Avenir", size: 11.0) ?? .systemFont(ofSize: 11.0)
    static let text = UIFont(name: "Avenir", size: 16.0) ?? .systemFont(ofSize: 16.0)
    static let emoji = UIFont.systemFont(ofSize: 96.0)
    
}

