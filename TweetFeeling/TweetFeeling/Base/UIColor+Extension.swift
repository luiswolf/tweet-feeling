//
//  UIColor+Extension.swift
//  TweetFeeling
//
//  Created by Luis Emilio Dias Wolf on 18/03/21.
//

import UIKit

extension UIColor {
    
    static let black = UIColor(named: "Black") ?? .black
    static let blue = UIColor(named: "Blue") ?? .blue
    static let gray = UIColor(named: "Gray") ?? .gray
    static let lightGray = UIColor(named: "LightGray") ?? .lightGray
    static let extraLightGray = UIColor(named: "ExtraLightGray") ?? .lightGray
    static let white = UIColor(named: "White") ?? .white
    
}
